# iOS Assignment CS

This is a placeholder README file with the instructions for the assingment. We expect you to build your own README file.

## Approach

### UI
* I've used .xib file and storyboard to layout UI with this design.
* ViewController class will display information about movies. This file also includes UITableView which is used for displaying movies.
* I've also written a "UIImageView" extension - which will download image from its URL. After downloading has completed, I will resize the image and use iOS default NSCache to cache data. In order to make smooth scrolling, when user scrolls to the end of the page, I will display load more UITableCell and call API service to get movies with page index starting from 1.
* MovieDetailsViewController will display more details about the movie. After viewDidLoad event, I will call API service to get movie details and display the movie. Movie instance will be passed after user clicked Movie cell.

### Service Repository
In this file I have "BaseMovieService" protocol that defines API request functions. "MovieService" class will implement this protocol.

### Model
I've defined Movie class which inherits "Codable" in order to decode JSON response from API services.

### ViewModel
This is the file to communicate between UIViewController and Services. This class has "BaseMovieService" instance which is used to fetch movies from network. After receiving data from service, I will store "Movie" and "Popular" list in ViewModel's properties.

### Rating View
* Used 2 CAShapeLayer to draw and convert colors of the rating view. Please note that rating view draw will be wiped and redrawn after each uitableviewcell reuse.

### Source
* https://bitbucket.org/weedigit/cs-ios-assignment/src/master/

You can find all the instrucrtions for the assingment on [Assingment Instructions](https://docs.google.com/document/d/1zCIIkybu5OkMOcsbuC106B92uqOb3L2PPo9DNFBjuWg/edit?usp=sharing).

## Delivering the code
* Fork this repo and select the access level as private **[Check how to do it here](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html)**
* Go to settings and add the user **m-cs-recruitment@backbase.com** with the read access **[Check how to do it here](https://confluence.atlassian.com/bitbucket/grant-repository-access-to-users-and-groups-221449716.html)**
* Send an e-mail to **m-cs-recruitment@backbase.com** with your info and repo once the development is done

Please remember to work with small commits, it help us to see how you improve your code :)
