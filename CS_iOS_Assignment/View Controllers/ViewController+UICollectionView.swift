//
//  ViewController+UICollectionView.swift
//  CS_iOS_Assignment
//
//  Created by Nguyen Thanh Duc on 30/09/2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation
import UIKit

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.movieVM.movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let weCell = collectionView.dequeueReusableCell(withReuseIdentifier: "we_colleciton_cell", for: indexPath) as! WEMovieCollectionViewCell
        weCell.index = indexPath
        let movie = self.movieVM.movies[indexPath.row]
        
        weCell.configure(movie: movie)
        
//        weCell.imgPoster.image = nil
//        Utils.shareInstance.loadImageUsingCache(withUrl: Constants.IMAGE_URL + movie.Poster) { (img) in
//            DispatchQueue.main.async {
//                if let aCell = collectionView.cellForItem(at: indexPath) as? WEMovieCollectionViewCell {
//                    aCell.imgPoster.image = img
//                    aCell.setNeedsDisplay()
//                }
//            }
//        }
        

        return weCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let movie = self.movieVM.movies[indexPath.row]
        self.navigationToDetails(movie: movie)
    }
}
