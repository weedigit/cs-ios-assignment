//
//  ViewController.swift
//  CS_iOS_Assignment
//
//  Copyright © 2019 Backbase. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var isLoadMore: Bool = false
    var isEndData: Bool = false
    
    @IBOutlet weak var moviesTableView: UITableView!
    
    let movieVM: MovieViewModel = MovieViewModel()
    
    lazy var headerView: WEHeaderView = {
        let v = UINib(nibName: "WEHeaderView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! WEHeaderView
        
        return v
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
        self.initUI()
    }
    
    fileprivate func initUI() {
        self.moviesTableView.register(UINib(nibName: "WEMovieTableViewCell", bundle: nil), forCellReuseIdentifier: "we_movie_cell")
        self.moviesTableView.register(UINib(nibName: "WEPopularTableViewCell", bundle: nil), forCellReuseIdentifier: "we_popular_cel")
        self.moviesTableView.register(UINib(nibName: "WEHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "we_header_view")
        self.moviesTableView.register(UINib(nibName: "WELoadMoreTableViewCell", bundle: nil), forCellReuseIdentifier: "we_load_more_cell")
        
        self.moviesTableView.delegate = self
        self.moviesTableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func fetchData() {
        self.movieVM.getMovies { (results) in
            switch results {
            case .success(_):
                self.moviesTableView.reloadData()
            case .failure(_):
                break
            }
        }
        
        self.movieVM.getPopular { (results) in
            switch results {
            case .success(_):
                self.moviesTableView.reloadData()
            case .failure(_):
                break
            }
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func navigationToDetails(movie: Movie) {
        let vc = self.storyboard?.instantiateViewController(identifier: "WEMovieDetailsViewController") as! WEMovieDetailsViewController
        vc.modalPresentationStyle = .overCurrentContext
        vc.movie = movie
        present(vc, animated: true, completion: nil)
    }
}
