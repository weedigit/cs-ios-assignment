//
//  WEPopularTableViewCell.swift
//  CS_iOS_Assignment
//
//  Created by Nguyen Thanh Duc on 30/09/2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import UIKit

class WEPopularTableViewCell: UITableViewCell {

    @IBOutlet weak var tbCollection: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.tbCollection.register(UINib(nibName: "WEMovieCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "we_colleciton_cell")
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCollectionViewDataSourceDelegate(dataSourceDelegate: UICollectionViewDataSource & UICollectionViewDelegate, forRow row: Int) {
        tbCollection.delegate = dataSourceDelegate
        tbCollection.dataSource = dataSourceDelegate
        tbCollection.tag = row
        tbCollection.reloadData()
    }
    
}
