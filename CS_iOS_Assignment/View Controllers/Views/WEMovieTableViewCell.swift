//
//  WEMovieTableViewCell.swift
//  CS_iOS_Assignment
//
//  Created by Nguyen Thanh Duc on 30/09/2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import UIKit

class WEMovieTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var rating: RatingView!
    @IBOutlet weak var releaseDate: UILabel!
    @IBOutlet weak var poster: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.poster.layer.borderWidth = 2
        self.poster.layer.borderColor = UIColor.gray.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.poster.image = nil
        self.rating.clearLayer()
    }
    
    func configure(movie: Movie) {
        title.text = movie.Title
        releaseDate.text = Utils.shareInstance.convertDate(inputDate: movie.ReleaseDate) 
        poster.image = nil
        rating.buildRating(rating: movie.Rating)
        self.poster.loadImageUsingCache(withUrl: Constants.IMAGE_URL + movie.Poster)
    }
    
}
