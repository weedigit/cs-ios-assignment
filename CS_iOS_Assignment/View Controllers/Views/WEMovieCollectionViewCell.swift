//
//  WEMovieCollectionViewCell.swift
//  CS_iOS_Assignment
//
//  Created by Nguyen Thanh Duc on 30/09/2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import UIKit

class WEMovieCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgPoster: UIImageView!
    var index: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imgPoster.image = nil
    }

    func configure(movie: Movie) {
        imgPoster.loadImageUsingCache(withUrl: Constants.IMAGE_URL + movie.Poster)
    }
    
}
