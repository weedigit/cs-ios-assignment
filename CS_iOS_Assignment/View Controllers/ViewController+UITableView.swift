//
//  ViewController+UITableView.swift
//  CS_iOS_Assignment
//
//  Created by Nguyen Thanh Duc on 30/09/2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation
import UIKit

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 160
        }
        return 100
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let weHeader = tableView.dequeueReusableHeaderFooterView(withIdentifier: "we_header_view") as! WEHeaderView
        weHeader.backgroundView?.backgroundColor = .red
        if section == 0 {
            weHeader.lblTitle.text = "Playing now"
        }else {
            weHeader.lblTitle.text = "Most popular"
        }
        return weHeader
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        if self.movieVM.popular.count > 0 {
            return self.movieVM.popular.count + 1
        }else {
            return self.movieVM.popular.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let weCell = tableView.dequeueReusableCell(withIdentifier: "we_popular_cel", for: indexPath) as! WEPopularTableViewCell
            weCell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forRow: indexPath.row)

            return weCell
        }else {
            if indexPath.row >= self.movieVM.popular.count && !self.isEndData {
                let weCell = tableView.dequeueReusableCell(withIdentifier: "we_load_more_cell", for: indexPath) as! WELoadMoreTableViewCell
                
                return weCell
            }else {
                let weCell = tableView.dequeueReusableCell(withIdentifier: "we_movie_cell", for: indexPath) as! WEMovieTableViewCell
                
                let movie = self.movieVM.popular[indexPath.row]
                weCell.configure(movie: movie)
                
                return weCell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.isKind(of: WELoadMoreTableViewCell.self) {
            let weCell = cell as! WELoadMoreTableViewCell
            weCell.waiting.startAnimating()
            
            if !self.isLoadMore && !self.isEndData {
                self.isLoadMore = true
                self.movieVM.getPopular { (results) in
                    switch results {
                    case .success(let data):
                        if data.count == 0 {
                            self.isEndData = true
                        }
                        DispatchQueue.main.async {
                            self.moviesTableView.reloadData()
                        }
                        break
                    case .failure(_):
                        break
                    }
                    self.isLoadMore = false
                }
                // Call load more
            }else {
                weCell.waiting.stopAnimating()
                self.isEndData = false
                self.isLoadMore = false
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.isKind(of: WELoadMoreTableViewCell.self) {
            let weCell: WELoadMoreTableViewCell = cell as! WELoadMoreTableViewCell
            weCell.waiting.stopAnimating()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movie = self.movieVM.popular[indexPath.row]
        self.navigationToDetails(movie: movie)
    }
}
