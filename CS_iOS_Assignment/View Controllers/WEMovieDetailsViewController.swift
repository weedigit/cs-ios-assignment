//
//  WEMovieDetailsViewController.swift
//  CS_iOS_Assignment
//
//  Created by Nguyen Thanh Duc on 01/10/2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import UIKit

class WEMovieDetailsViewController: UIViewController {

    @IBOutlet weak var imgPoster: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblReleaseDate: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var tagsView: UIView!
    @IBOutlet weak var mainScroll: UIScrollView!
    
    var movie: Movie?
    var tagsArray:[Genre] = Array()
    let viewModel: MovieViewModel = MovieViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imgPoster.layer.borderWidth = 4
        self.imgPoster.layer.borderColor = UIColor.gray.cgColor

        // Do any additional setup after loading the view.
        guard let mv = movie else {
            return
        }
        
        self.lblName.text = mv.Title
        self.lblReleaseDate.text = Utils.shareInstance.convertDate(inputDate: mv.ReleaseDate)
        self.lblContent.text = mv.OverView
        self.imgPoster.loadImageUsingCache(withUrl: Constants.IMAGE_URL + movie!.Poster)
        
        self.viewModel.getMovieDetails(movieID: String(mv.ID)) { (results) in
            switch results {
            case .success(let movie):
                guard let g = movie.Genres else {
                    return
                }
                self.tagsArray = g
                self.createTagCloud(OnView: self.tagsView, withArray: self.tagsArray)
                let (h, m, _) = Utils.shareInstance.secondsToHoursMinutesSeconds(seconds: movie.Runtime * 60)
                self.lblReleaseDate.text = Utils.shareInstance.convertDate(inputDate: mv.ReleaseDate) + "  -  \(h)h \(m)m"
                break
            case .failure(let err):
                break
            }
        }
    }
    
    deinit {
        print("remove details view")
    }
    
    @IBAction func btnCloseClick(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func createTagCloud(OnView view: UIView, withArray data:[Genre]) {
        for tempView in view.subviews {
            if tempView.tag != 0 {
                tempView.removeFromSuperview()
            }
        }
        
        var xPos:CGFloat = 15.0
        var ypos: CGFloat = 0.0
        var tag: Int = 1
        for genre in data  {
            let startstring = genre.Name
            let width = startstring.widthOfString(usingFont: UIFont(name:"verdana", size: 13.0)!)
            let checkWholeWidth = CGFloat(xPos) + CGFloat(width) + CGFloat(13.0) + CGFloat(15.5 )//13.0 is the width between lable and cross button and 25.5 is cross button width and gap to righht
            if checkWholeWidth > UIScreen.main.bounds.size.width - 35.0 {
                //we are exceeding size need to change xpos
                xPos = 15.0
                ypos = ypos + 29.0 + 8.0
            }
            
            let bgView = UIView(frame: CGRect(x: xPos, y: ypos, width:width + 17.0 + 20 , height: 29.0))
            bgView.layer.cornerRadius = 14.5
            bgView.backgroundColor = UIColor.white
            bgView.tag = tag
            
            let textlable = UILabel(frame: CGRect(x: 17.0, y: 0.0, width: width, height: bgView.frame.size.height))
            textlable.font = UIFont(name: "verdana", size: 13.0)
            textlable.text = startstring
            textlable.textColor = UIColor.black
            bgView.addSubview(textlable)
            
            xPos = CGFloat(xPos) + CGFloat(width) + CGFloat(17.0) + CGFloat(30.0)
            view.addSubview(bgView)
            tag = tag  + 1
        }
        
    }
        
    @objc func removeTag(_ sender: AnyObject) {
        tagsArray.remove(at: (sender.tag - 1))
        createTagCloud(OnView: self.view, withArray: tagsArray as [Genre])
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
