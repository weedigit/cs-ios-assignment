//
//  MovieViewModel.swift
//  CS_iOS_Assignment
//
//  Created by Nguyen Thanh Duc on 29/09/2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation


class MovieViewModel {
    
    var pageIndex: Int = 0
    
    let movieService: BaseMovieService!
    
    var movies: [Movie] = [Movie]()
    var popular: [Movie] = [Movie]()
    
    init(service: BaseMovieService = MovieService()) {
        self.movieService = service
    }
    
    /// Fetch Movies
    /// - Parameter completionHandler: Result [Movie] or Error
    func getMovies(completionHandler: @escaping (Result<[Movie], Error>) -> Void) {
        self.movieService.fetchMovies { (result) in
            switch result {
            case .success(let data):
                self.movies = data
                break
            case .failure(_):
                break
            }
            completionHandler(result)
        }
    }
    
    
    /// Fetch Popular movie
    /// - Parameter completionHandler: Result [Movie] or Error
    func getPopular(completionHandler: @escaping(Result<[Movie], Error>) -> Void) {
        pageIndex += 1
        self.movieService.fetchPopulars(pageIndex: pageIndex) { (results) in
            switch results {
            case .success(let data):
                self.popular.append(contentsOf: data)
                break
            case .failure(_):
                break
            }
            completionHandler(results)
        }
    }
    
    /// Fetch Movie Details
    /// - Parameters:
    ///   - movieID: MovieID String
    ///   - completionHandler: Result Movie or Error
    func getMovieDetails(movieID: String, completionHandler: @escaping(Result<Movie,Error>) -> Void) {
        self.movieService.fetchMovieDetails(movieID: movieID) { (results) in
            completionHandler(results)
        }
    }
}
