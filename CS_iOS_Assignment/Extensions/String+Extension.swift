//
//  String+Extension.swift
//  CS_iOS_Assignment
//
//  Created by Nguyen Thanh Duc on 01/10/2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation
import UIKit

extension String {
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
}
