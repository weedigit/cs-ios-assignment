//
//  RatingView.swift
//  CS_iOS_Assignment
//
//  Copyright © 2019 Backbase. All rights reserved.
//

import UIKit

class RatingView: UIView {
    lazy var lblRating: UILabel = {
        let v = UILabel()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.textColor = UIColor.white
        v.font = UIFont.boldSystemFont(ofSize: 14)
        v.backgroundColor = .clear
        return v
    }()
    
    lazy var lblPercent: UILabel = {
        let v = UILabel()
        v.text = "%"
        v.translatesAutoresizingMaskIntoConstraints = false
        v.textColor = UIColor.white
        v.font = UIFont.boldSystemFont(ofSize: 6)
        v.backgroundColor = .clear
        return v
    }()
    
    let circleLayer = CAShapeLayer()
    let shapeLayer = CAShapeLayer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initSubviews()
    }
    
    func initSubviews() {
        drawBackground()
        self.addSubview(lblRating)
        self.addSubview(lblPercent)
        NSLayoutConstraint.activate([
            lblRating.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            lblRating.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            lblPercent.topAnchor.constraint(equalTo: lblRating.topAnchor, constant: 0),
            lblPercent.leadingAnchor.constraint(equalTo: lblRating.trailingAnchor, constant: 0)
        ])
    }
    
    func clearLayer() {
        circleLayer.removeFromSuperlayer()
        shapeLayer.removeFromSuperlayer()
    }
    
    func buildRating(rating: Double) {
        lblRating.text = "\(Int(rating * 10))"
        
        drawCircleShape()
        
        let path = UIBezierPath(arcCenter: CGPoint(x: self.bounds.midX, y: self.bounds.midY), radius: self.bounds.width/2 - 5, startAngle: -CGFloat.pi / 2, endAngle: (CGFloat(rating) * CGFloat.pi * 2)/10 - CGFloat.pi / 2, clockwise: true)

        shapeLayer.path = path.cgPath
        shapeLayer.lineWidth = 5.0
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = UIColor(named: "RatingGreenColor")?.cgColor
        if (rating < 5) {
            shapeLayer.strokeColor = UIColor(named: "RatingYellowColor")?.cgColor
            circleLayer.strokeColor = UIColor(named: "RatingYellowBgColor")?.cgColor
        }
        
        self.layer.addSublayer(shapeLayer)
    }
    
    func drawBackground() {
        let shapeLayer = CAShapeLayer()
        let path = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.bounds.width/2)
        
        shapeLayer.path = path.cgPath
        shapeLayer.lineWidth = 1.0
        shapeLayer.strokeColor = UIColor(named: "RatingColor")?.cgColor
        shapeLayer.fillColor = UIColor(named: "RatingColor")?.cgColor
        self.layer.addSublayer(shapeLayer)
    }
    
    func drawCircleShape() {
        let path = UIBezierPath(arcCenter: CGPoint(x: self.bounds.midX, y: self.bounds.midY), radius: self.bounds.width/2 - 5, startAngle: 0.0, endAngle: CGFloat.pi * 2, clockwise: true)
        
        circleLayer.path = path.cgPath
        circleLayer.lineWidth = 5.0
        circleLayer.strokeColor = UIColor(named: "RatingGreenBgColor")?.cgColor
        circleLayer.fillColor = UIColor.clear.cgColor
        self.layer.addSublayer(circleLayer)
    }
}
