//
//  Utils.swift
//  CS_iOS_Assignment
//
//  Created by Nguyen Thanh Duc on 30/09/2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation
import UIKit

class Utils {
    static let shareInstance: Utils = {
        let instance = Utils()
        
        return instance
    }()
    
    func loadImageUsingCache(withUrl urlString : String, completionHandler: @escaping(UIImage?) -> Void) {
            let url = URL(string: urlString)
            if url == nil {
                completionHandler(nil)
                return
            }
            
            // check cached image
            if let cachedImage = imageCache.object(forKey: urlString as NSString)  {
                completionHandler(cachedImage)
                return
            }

            // if not, download image from url
            URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
                if error != nil {
                    print(error!)
                    return
                }

                if let image = UIImage(data: data!) {
                    let resizeImage = image.resizeImageWith(newSize: CGSize(width: 100, height: 75))
                    imageCache.setObject(resizeImage, forKey: urlString as NSString)
                    DispatchQueue.main.async {
                        completionHandler(resizeImage)
                    }
                }
            }).resume()
        }
    
    func convertDate(inputDate: String) -> String {
        if (inputDate.isEmpty || inputDate.count != 10) {
            return inputDate
        }
         let olDateFormatter = DateFormatter()
         olDateFormatter.dateFormat = "yyyy-MM-dd"
         let oldDate = olDateFormatter.date(from: inputDate)
         let convertDateFormatter = DateFormatter()
         convertDateFormatter.dateFormat = "MMM dd, yyyy"
         return convertDateFormatter.string(from: oldDate!)
      }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
      return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
}
