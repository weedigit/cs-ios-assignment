//
//  Constants.swift
//  CS_iOS_Assignment
//
//  Created by Nguyen Thanh Duc on 29/09/2020.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation

class Constants {
    static public var BASE_URL = URL(string: "https://api.themoviedb.org")!
    static let IMAGE_URL = "https://image.tmdb.org/t/p/original"
    static public let API_KEY = "55957fcf3ba81b137f8fc01ac5a31fb5"
}
